#ifndef MARKERS_H_
#define MARKERS_H_

/*-------------------------------- Includes ---------------------------------*/

#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <set>

#include "utils.h"
#include "params.h"

#define QTLL_HEADER "QTL\tmrk_list"
#define QTLW_HEADER "QTL\twindow"
#define QTLN_HEADER "QTL\tmrk_nb"
#define MRK_QTL_HEADER "Locus\tChr\tQTL\tPos\tAll+"

/*---------------------------- Public structures ----------------------------*/

typedef struct s_mk{

	char name[NB_STRING_MAX]; // Name of the marker/QTL
	int chr; // Chromosome number of the maker/QTL localization
	int num_qtl; // Index of the QTL which the marker is associated to
	double pos; // Position of the marker in the chromosome
	int nb_all_fav; // Number of favorable alleles
	char **all_fav; // Name of the favorable alleles at QTL

} s_mk;

typedef struct {char mrk[NB_STRING_MAX]; double pos;} mrkpos;


/*---------------------------- Public prototypes ----------------------------*/

bool stl_comp_mrkpos(const mrkpos & m1, const mrkpos & m2);

void fill_qtl(s_params* params, s_mk & marker, char** const qp_fields, double qtlpos);

void fill_mk(s_params* params, s_mk & marker, char** const qp_fields, const mrkpos & mp);

s_mk* assign_mrks_to_qtlw(s_params* params, s_mk* markers, const std::vector<mrkpos>& v_mrkpos, char** const qp_fields, char** const qw_fields);

s_mk* assign_mrks_to_qtln(s_params* params, s_mk* markers, const std::vector<mrkpos>& v_mrkpos, char** const qp_fields, char** const qn_fields);

s_mk* assign_mrks_to_qtll(s_params* params, s_mk* markers, const std::vector<mrkpos>& v_mrkpos, char** const qp_fields, char** const ql_fields);

s_mk *init_tab_mks(s_params *params);

s_mk *init_tab_mks1(s_params *params);

s_mk *init_tab_mks2(s_params *params, const char* file_qp);

void free_markers(s_mk *markers, int nb_mks);

void print_tab_mks(s_mk *markers, int nb_mks);

void dump_tab_mks(s_mk *markers, s_params *params);

void find_index(s_mk *markers, s_params *params, int q);

void compute_rec_frac(s_mk* markers, s_params* params);

int is_fav(const char *allele, s_mk *markers, int index_mk);

void check_fav_alleles_input_files(s_mk *markers, s_params *params);


#endif /* MARKERS_H_ */
