Package: optimaRs
Title: A Decision Support Tool to conduct Marker-Assisted Recurrent Selection Programs
Version: 1.6
Authors@R:
    c(person("Franck", "Gauthier", ,"franck.gauthier@inrae.fr", role = c("aut", "cre")),
      person("Fabio", "Valente", ,, role = c("aut")),
      person("Yannick", "De-Oliveira", ,"<yannick.de-oliveira@inrae.fr>", role = c("aut")),
      person("Sarah", "Ben Sadoun", ,"sarah.ben-sadoun@agroparistech.fr", role = c("aut")),
      person("Alain", "Charcosset", ,"alain.charcosset@inrae.fr", role = c("aut")),
      person("Laurence", "Moreau", ,"laurence.moreau@inrae.fr", role = c("aut")))
Description: Decision support tools to help breeders in implementing their Marker-Assisted Selection (MAS) project in a multi-allelic context. OptimaRs provides algorithms to trace parental QTL alleles identified as favorable throughout selection generations, using information given by markers located in the vicinity of the estimated QTL positions. Using these results, probabilities of allele transmission are computed in different MAS schemes and mating designs (intercrossing, selfing, backcrossing, double haploids, RIL) with the possibility of considering generations without genotypic information. Then, strategies are proposed to select the best plants and to efficiently intermate them based on the expected value of their progenies.
License: GPL (>= 3)
Encoding: UTF-8
Roxygen: list(markdown = TRUE)
NeedsCompilation: yes
RoxygenNote: 7.3.2
Imports: 
    DOT,
    ggplot2,
    Rcpp,
    reshape2,
    rsvg
LinkingTo: 
    Rcpp
