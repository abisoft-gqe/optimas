
library(optimaRs)

# show the list of available objects (functions and data) in package optimaRs.
# getNamespaceExports("optimaRs")

# NB: Set data_dir to the place where you downloaded and extracted data.zip
data_dir = Sys.getenv("HOME")

test_dir = paste0(data_dir,"/data/optimaRs_test")
setwd(test_dir)

################################################################################
#                     STEP 1 - PROBABILITY PREDICTION                          #
################################################################################
# system.time({
#   system("optimas ~/data/optimaRs_test/input/blanc.dat ~/data/optimaRs_test/input/blanc.map 0.000001 0.0 ~/tmp/optimas_output/result_dir6 0")
# })

# system.time({
l_optimars_session = optimars.prediction(dat_file="input/blanc.dat",
                                         map_file="input/blanc.map", 
                                         haplo_cutoff=0.000001, gam_cutoff=0.0,
                                         ae_file="input/allelic_effects_blanc.txt",
                                         output_dir="output/results_blanc1", qtl = 0)
 # })
# Uncomment the folling line to reload prediction results previously saved in text files
# l_optimars_session = optimars.reloadPredictionResults("output/results_blanc1/2023_05_12_01_22_42/")

# A more concise way to access the main data frames
df_tab_parents = l_optimars_session$prediction_results$df_parents
df_tab_score = l_optimars_session$prediction_results$df_score
# get output_dir
output_dir = l_optimars_session$output_dir

# NB: It is better to load allelic effects and compute PMS during the prediction process above,
# However, you may do it afterward (uncomment the following three lines).
# pms_mat = optimars.computePmsFromAllelicEffects(df_tab_parents, "input/allelic_effects_blanc.txt")
# l_optimars_session$prediction_results$df_score = cbind(df_tab_score, pms_mat)
# df_tab_score = l_optimars_session$prediction_results$df_score

# Exported image properties using function ggsave()
ggsave_width=1600; ggsave_height=1200; ggsave_unit="px"; ggsave_dpi=ggsave_width/10; ggsave_path=test_dir; ggsave_filename=paste0(output_dir,"/optimars_plot.png")

##### SCORE HISTOGRAM ######
histo = optimars.scoreHisto(df_tab_score, column="MS")
histo
ggsave(ggsave_filename, plot=histo, path=ggsave_path, width=ggsave_width, height=ggsave_height, unit=ggsave_unit, dpi=ggsave_dpi)
# Provisional alternative to ggsave + png font problem
# png(ggsave_filename, width=ggsave_width, height=ggsave_height, res=ggsave_dpi, units=ggsave_unit)
# print(histo)
# dev.off()

################################################################################
#                   STEP 2 - INDIVIDUAL SELECTION METHODES                     #
################################################################################

####### A - MANUAL SELECTION #######

individuals = c("B124", "B125","B13","B158","B242")
# Create a selection data frame by selecting corresponding rows in the main score data frame
df_manual_list = optimars.addToList(df_tab_score, individuals)
# NB: This manual list can be added to an existing selection list data frame,
# e.g. df_dest_list1 in the commented example below.
# df_dest_list1 = optimars.addToList(df_tab_score, individuals, df_dest_list1)

####### B - TRUNCATION SELECTION #######

# Select the 8 best individuals (whose cycle is C2) based on their Molecular Score (MS)
df_list_truncation1 = optimars.truncationSelection( df_tab_score, nb_of_indiv=8, crit="MS", cycle="C2" )

####### C - QTL COMPLEMENTATION SELECTION ######

# Completes df_list_truncation1 with supplementary individuals regarding the favorable alleles they carry
df_list_complementation1 = optimars.QtlComplementationSelection( df_tab_score, teta_ms=0.47, nT=2, MSmin=0.70, Nmax=10, cycle="C2", df_selection_list=df_list_truncation1 )

##### SCORE HISTOGRAM ######
histo = optimars.scoreHisto(df_list_complementation1, column="MS")
histo
# uncomment the line below to export histo to file <output_dir>"/optimars_plot.png"
# ggsave(ggsave_filename, plot=histo, path=ggsave_path, width=ggsave_width, height=ggsave_height, unit=ggsave_unit, dpi=ggsave_dpi)

##### SCORE BOXPLOT BY GROUP ######
boxplot = optimars.scoreGroupBoxPlot(df_tab_score, column="MS")
boxplot
# ggsave(ggsave_filename, plot=boxplot, path=ggsave_path, width=ggsave_width, height=ggsave_height, unit=ggsave_unit, dpi=ggsave_dpi)

##### graph showing the score mean by cycle for MS (All) and each QTL #####
joined_histo = optimars.scoreMeanByCycleHisto(df_tab_score)
joined_histo
# ggsave(ggsave_filename, plot=joined_histo, path=ggsave_path, width=ggsave_width, height=ggsave_height, unit=ggsave_unit, dpi=ggsave_dpi)

################################################################################
#                   STEP 3 - INTERMATING                                       #
################################################################################
# identify the crosses to be made to initiate the next MARS cycle

df_homo_hetero = l_optimars_session$prediction_results$df_homo_hetero

# method better_half on df_list_complementation1
List1_Better_Half_from_QCS = optimars.intermating(df_homo_hetero, df_list1=df_list_complementation1, cross_method="better_half", crit="MS")
histocrossplot = optimars.scoreHisto(List1_Better_Half_from_QCS$df_crossed_individuals, bar_color="lightgreen")
histocrossplot
# ggsave(ggsave_filename, plot=histocrossplot, path=ggsave_path, width=ggsave_width, height=ggsave_height, unit=ggsave_unit, dpi=ggsave_dpi)
crossingplot = optimars.crossingSimulationPlot(List1_Better_Half_from_QCS)
crossingplot
# ggsave(ggsave_filename, plot=crossingplot, path=ggsave_path, width=ggsave_width, height=ggsave_height, unit=ggsave_unit, dpi=ggsave_dpi)

# method complete on df_list_complementation1
List2_Complete_from_QCS = optimars.intermating(df_homo_hetero, df_list_complementation1, cross_method="complete", self=FALSE, crit="MS")
crossingplot = optimars.crossingSimulationPlot(List2_Complete_from_QCS)
crossingplot
# method complete + selfing on df_list_complementation1
List3_Complete_selfing_from_QCS = optimars.intermating(df_homo_hetero, df_list_complementation1, cross_method="complete", self=TRUE, crit="MS")
crossingplot = optimars.crossingSimulationPlot(List3_Complete_selfing_from_QCS)
crossingplot

# method factorial design (2 lists)
List4_Factorial_Design = optimars.intermating(df_homo_hetero, df_manual_list, df_list2=df_list_truncation1, crit="MS")
crossingplot = optimars.crossingSimulationPlot(List4_Factorial_Design)
crossingplot

# max_ind_contrib = 1 and criterion = MS_UC on df_list_complementation1
List5_UC_Contrib_1_from_QCS = optimars.intermating(df_homo_hetero, df_list_complementation1, max_ind_contrib=1, crit="MS_UC")
crossingplot = optimars.crossingSimulationPlot(List5_UC_Contrib_1_from_QCS)
crossingplot
# max_ind_contrib = 1 and criterion = MS on df_list_complementation1
List6_MS_Contrib_1_from_QCS = optimars.intermating(df_homo_hetero, df_list_complementation1, max_ind_contrib=1, crit="MS")
crossingplot = optimars.crossingSimulationPlot(List6_MS_Contrib_1_from_QCS)
crossingplot


################################################################################
#                         WEIGHT QTL                                           #
################################################################################

# qtlWeightCoef: An integer vector containing the QTL coefficients. Its length has to be equal to the number of QTLs.
qtlWeightCoef = c(3,1,1,1,1,1,1,1,1,1,1)
v_ms_weight = optimars.computeMSWeight(df_list_complementation1, qtlWeightCoef)
# update the MS_Weight column in the score data frame
df_list_complementation1$MS_Weight = v_ms_weight



################################################################################
#                   COMPUTE AN INDEX                                           #
################################################################################

# index mathematical expression (formula) including some score data frame column name (e.g. MS and MS_UC)
formula_expr = "MS*2+log(MS_UC)"
formula_name = "my_index"

### Compute the index, check for errors, then insert the index column in df_list_complementation1.
df_list_complementation1 = optimars.computeAndInsertIndex(df_list_complementation1, formula_expr, formula_name)
# NB: If an error occurs, df_list_complementation1 is returned unchanged.
# NB2: This function computes/inserts the index column for a single score dataframe (coming from a prediction/selection/intermating step),
#      so you may have to re-call it on other score dataframes according to your needs.

# ### Step by step method: 
# #   1 - Compute index. This does not insert any index column in df_list_complementation1.
# #   2 - Check for errors.
# #   3 - Insert the index column in df_list_complementation1
# v_result = optimars.computeIndex(df_list_complementation1, formula_expr)
# ### If no computation problems occurred insert the index column in the data frame 
# # Invoke geterrmessage() to get the error message when this function fails i.e. returns NULL.
# if (is.null(v_result)) {
#   message(geterrmessage())
# } else {
#   # Check for NaN or infinite values
#   if(any(!is.finite(v_result))) {
#     message("Warning, Some results are not finite !")
#   } else {
#     # insert the index column in the score data frame
#     df_list_complementation1 = optimars.insertComputedIndex(df_list_complementation1, v_result, formula_name)
#   }
# }

################################################################################
#                   DRAW PEDIGREE GRAPH                                        #
################################################################################

v_indiv = df_list_complementation1$"Id"
# v_indiv2 = c(v_indiv, c("C1", "A23"))
df_all_parents = df_tab_score[,c("Id","P1","P2")]

# get all pair "parent -> child" involved in the pedigree
v_pedigree = optimars.get.pedigree(v_indiv, df_all_parents)

# Create the pedigree as a DAG in SVG format (xml string) using the DOT package.
svg_xml_str = optimars.draw.pedigree(v_pedigree, v_indiv)
# svg_xml_str = optimars.draw.pedigree(v_pedigree, v_colored_indiv = v_indiv2, color = "red")

# export the pedigree to PNG, SVG and PDF
success = optimars.export.pedigree(svg_xml_str, paste0(output_dir,"/optimars_pedigree1.png"), height = 1600)
success = optimars.export.pedigree(svg_xml_str, paste0(output_dir,"/optimars_pedigree1.svg"), height = 1600)
success = optimars.export.pedigree(svg_xml_str, paste0(output_dir,"/optimars_pedigree1.pdf"), height = 1600)

###################


