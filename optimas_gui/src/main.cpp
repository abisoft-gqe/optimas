#define GLOBAL

#include <QtCore>
#include <QCoreApplication>
#include <QtWidgets>
#include "../headers/MainWindow.h"
#include "../headers/utils.h"

using namespace std;

int main(int argc, char *argv[])
{
	//Q_INIT_RESOURCE(configdialog);
	
	//QTextCodec::setCodecForTr(QTextCodec::codecForLocale()); // removed when upgrading to QT5
	QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
	//QTextCodec::setCodecForCStrings(QTextCodec::codecForLocale()); // removed when upgrading to QT5
	
	// harmless but anoying messages (below) may happen when launching optimas_gui from a terminal. 
	//e.g.: t.qpa.xcb: QXcbConnection: XCB error: 3 (BadWindow)
	//// TRY: Uncomment the following line to suppress them
	//qputenv("QT_LOGGING_RULES","*.debug=false;qt.qpa.*=false");

#ifdef _WIN32
	QApplication::setStyle("fusion");
#else
	QApplication::setStyle("cleanlooks");
#endif
	QApplication app(argc, argv);

	MainWindow main_window;
	main_window.show();

	return app.exec();
}
