#include "../headers/HistogramItem.h"

#include <qstring.h>
//#include <qpainter.h>
#include <qwt_plot.h>
//#include <qwt_interval_data.h>
//#include <qwt_painter.h>
//#include <qwt_scale_map.h>

void HistogramItem::setColor(const QColor & color)
{
    //QColor c = color;
    //c.setAlpha( 180 ); // transparency
    //setBrush( QBrush( c ) );
    setBrush( QBrush( color ) );
}

HistogramItem::HistogramItem( const QString & title ): QwtPlotHistogram(title) {

    setStyle( QwtPlotHistogram::Columns );

}

//~ HistogramItem::~HistogramItem(){;}

void HistogramItem::setValues( uint numValues, const double *values )
{
    QVector<QwtIntervalSample> samples( numValues );
    for ( uint i = 0; i < numValues; i++ )
    {
        QwtInterval interval( double( i ), i + 1.0 );
        interval.setBorderFlags( QwtInterval::ExcludeMaximum );

        samples[i] = QwtIntervalSample( values[i], interval );
    }

    setData( new QwtIntervalSeriesData( samples ) );
}

void HistogramItem::setValues( const QVector<QwtInterval> & intervals, const QVector<double> & values )
{
    uint numValues = intervals.size();
    //~ if(numValues != values.size()) return false;
    
    QVector<QwtIntervalSample> samples( numValues );
    for ( uint i = 0; i < numValues; i++ )
    {
        samples[i] = QwtIntervalSample( values[i], intervals[i] );
    }
    setData( new QwtIntervalSeriesData( samples ) );
    
    //~ return true;
}
