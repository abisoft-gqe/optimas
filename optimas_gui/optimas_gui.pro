TEMPLATE = app
TARGET = optimas_gui

# QT4
#QT += core gui
#QT *= svg

# QT5
QT += widgets

# qDebug: CONFIG += console
linux-g++|linux-g++-32|linux-g++-64 { 
	INCLUDEPATH += /usr/include/qwt/ /usr/include/graphviz/
	LIBS += -lqwt-qt5 -lgvc -lcgraph
	QMAKE_CXXFLAGS_RELEASE -= -g
	QMAKE_CFLAGS_RELEASE -= -g
}
#macx {
#    INCLUDEPATH += /opt/local/include/         /opt/local/include/qwt/         /opt/local/include/graphviz
#    LIBS += -L/opt/local/lib -lqwt -lgvc -lgraph #-lcdt #-lpathplan
#    LIBS += -L/opt/local/lib/graphviz -lgvplugin_dot_layout -lgvplugin_pango 
#    LIBS += -L/opt/local/lib/pango/1.6.0/modules/pango-basic-fc.so
#}
macx {
    INCLUDEPATH += /opt/local/include/         /usr/local/qwt-6.1.4/lib/qwt.framework/Headers         /opt/local/include/graphviz
    LIBS += -L/opt/local/lib -lgvc -lcdt -lpathplan -lcgraph
    LIBS += -L/opt/local/lib/graphviz -lgvplugin_dot_layout -lgvplugin_pango
    LIBS += /usr/local/qwt-6.1.4/lib/qwt.framework/qwt
}
win32 { 
    INCLUDEPATH += "C:/Qwt-6.1.4/include" "C:/Program Files/Graphviz/include/graphviz"
    LIBS += -L"C:/Qwt-6.1.4/lib" -lqwt -L"C:/Program Files/Graphviz/bin" -lgvc -lcgraph -lpathplan -lcdt -L"C:/Program Files/Graphviz/lib" -lgvc
    DEFINES += QWT_DLL
	RC_FILE = resource.rc
}

HEADERS += headers/FillMdWindow.h     headers/IndexWindow.h     headers/OImageViewer.h     headers/QtlWeightWindow.h     headers/HistoColorWindow.h     headers/TruncationOptionWindow.h     headers/CrossOptionWindow.h     headers/TableViewWindow.h     headers/FindDialog.h     headers/OTableWidget.h     headers/CompResultsWindow.h     headers/ComplementationWindow.h     headers/VisualizationWindow.h     headers/utils.h     headers/MainWindow.h     headers/LicenseWindow.h     headers/ImportWindow.h     headers/HistogramItem.h     headers/OBoxPlotItem.h     headers/AddListDialog.h     headers/AboutWindow.h     headers/muParserBase.h 	headers/muParserBytecode.h 	headers/muParserCallback.h 	headers/muParserDef.h 	headers/muParserError.h 	headers/muParserFixes.h 	headers/muParser.h 	headers/muParserStack.h 	headers/muParserTemplateMagic.h 	headers/muParserToken.h 	headers/muParserTokenReader.h
SOURCES += src/FillMdWindow.cpp     src/IndexWindow.cpp     src/OImageViewer.cpp     src/QtlWeightWindow.cpp     src/HistoColorWindow.cpp     src/TruncationOptionWindow.cpp     src/CrossOptionWindow.cpp     src/TableViewWindow.cpp     src/FindDialog.cpp     src/OTableWidget.cpp     src/CompResultsWindow.cpp     src/ComplementationWindow.cpp     src/VisualizationWindow.cpp     src/utils.cpp     src/MainWindow.cpp     src/main.cpp     src/LicenseWindow.cpp     src/ImportWindow.cpp     src/HistogramItem.cpp     src/OBoxPlotItem.cpp     src/AddListDialog.cpp     src/AboutWindow.cpp     src/muParserBase.cpp 	src/muParserBytecode.cpp 	src/muParserCallback.cpp 	src/muParser.cpp 	src/muParserError.cpp 	src/muParserTokenReader.cpp
FORMS += ui/fillMdWindow.ui     ui/indexWindow.ui     ui/histoColorWindow.ui     ui/qtlWeightWindow.ui     ui/truncationOptionWindow.ui     ui/crossOptionWindow.ui     ui/tableViewWindow.ui     ui/compResultsWindow.ui     ui/complementationWindow.ui     ui/visualizationWindow.ui     ui/aboutWindow.ui     ui/importWindow.ui     ui/licenseWindow.ui     ui/mainWindow.ui
RESOURCES += ui/configdialog.qrc

