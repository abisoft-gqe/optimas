/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 * Qwt Widget Library
 * Copyright (C) 1997   Josef Wilgen
 * Copyright (C) 2002   Uwe Rathmann
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Qwt License, Version 1.0
 *****************************************************************************/

#ifndef QWT_PLOT_TRADING_CURVE_H
#define QWT_PLOT_TRADING_CURVE_H

//~ #include "OBoxPlotSample.h"
#include <qwt_global.h>
#include <qwt_series_data.h>
#include <qwt_plot_seriesitem.h>
#include <qwt_samples.h>

#include <qvector.h>
#include <qrect.h>

/*!
   \brief Open-High-Low-Close sample used in boxplot charts

   In financial charts the movement of a price in a time interval is often
   represented by the opening/closing prices and the lowest/highest prices
   in this interval.

   \sa QwtTradingChartData
*/
class OBoxPlotSample
{
public:
    OBoxPlotSample(  double time = 0.0,
        double min = 0.0,
        double qrt1 = 0.0, double median = 0.0, double qrt3 = 0.0,
        double max = 0.0 );

    QwtInterval boundingInterval() const;

    bool isValid() const;

    /*!
      Time of the sample, usually a number representing
      a specific interval - like a day.
    */
    double time;
    
    double high;

    //! Opening price
    double q3;

    //! Highest price
    double med;

    //! Lowest price
    double q1;

    //! Closing price
    double low;
    
    //! TODO outliers
    // QVector<double> outliers;
};


/*!
  Constructor

  \param t Time value
  \param o Open value
  \param h High value
  \param l Low value
  \param c Close value
*/
inline OBoxPlotSample::OBoxPlotSample( double t,
        double min,
        double qrt1, double median, double qrt3,
        double max ):
    time( t ),
    high( max ),
    q3( qrt3 ),
    med( median ),
    q1( qrt1 ),
    low( min )
{
}

/*!
  \brief Check if a sample is valid

  A sample is valid, when all of the following checks are true:

  - low <= high
  - low <= open <= high
  - low <= close <= high

  \return True, when the sample is valid
 */
inline bool OBoxPlotSample::isValid() const
{
    return ( low <= q1 )
        && ( q1 <= med )
        && ( med <= q3 )
        && ( q3 <= high );
}

/*!
   \brief Calculate the bounding interval of the OHLC values

   For valid samples the limits of this interval are always low/high.

   \return Bounding interval
   \sa isValid()
 */
inline QwtInterval OBoxPlotSample::boundingInterval() const
{
    //~ double minY = open;
    //~ minY = qMin( minY, high );
    //~ minY = qMin( minY, low );
    //~ minY = qMin( minY, close );

    //~ double maxY = open;
    //~ maxY = qMax( maxY, high );
    //~ maxY = qMax( maxY, low );
    //~ maxY = qMax( maxY, close );

    //~ return QwtInterval( minY, maxY );
    return QwtInterval( low, high );
}

/*!
  \brief QwtPlotTradingCurve illustrates movements in the price of a
         financial instrument over time.

  QwtPlotTradingCurve supports candlestick or bar ( OHLC ) charts
  that are used in the domain of technical analysis.

  While the length ( height or width depending on orientation() )
  of each symbol depends on the corresponding OHLC sample the size
  of the other dimension can be controlled using:

  - setSymbolExtent()
  - setSymbolMinWidth()
  - setSymbolMaxWidth()

  The extent is a size in scale coordinates, so that the symbol width
  is increasing when the plot is zoomed in. Minimum/Maximum width
  is in widget coordinates independent from the zoom level.
  When setting the minimum and maximum to the same value, the width of
  the symbol is fixed.
*/

class QWT_EXPORT OBoxPlotItem: public QwtPlotSeriesItem, QwtSeriesStore<OBoxPlotSample>
{
public:
    /*!
        \brief Symbol styles.

        The default setting is QwtPlotSeriesItem::BoxAndWhisker.
        \sa setSymbolStyle(), symbolStyle()
    */
    enum SymbolStyle
    {
        //! Nothing is displayed
        NoSymbol = -1,

        /*!
          A line on the chart shows the price range (the highest and lowest
          prices) over one unit of time, e.g. one day or one hour.
          Tick marks project from each side of the line indicating the
          opening and closing price.
         */
        //~ Bar,

        /*!
          The range between q1/q3 are displayed as
          a filled box. The highest/lowest
          values are displayed as horizontal lines. The box is connected to the
          highest/lowest values by vertical lines.
        */
        BoxAndWhisker,

        /*!
          SymbolTypes >= UserSymbol are displayed by drawUserSymbol(),
          that needs to be overloaded and implemented in derived
          curve classes.

          \sa drawUserSymbol()
        */
        UserSymbol = 100
    };

    /*!
        \brief Direction of a price movement
     */
    //~ enum Direction
    //~ {
        //~ //! The closing price is higher than the opening price
        //~ Increasing,

        //~ //! The closing price is lower than the opening price
        //~ Decreasing
    //~ };

    /*!
        Attributes to modify the drawing algorithm.
        \sa setPaintAttribute(), testPaintAttribute()
    */
    enum PaintAttribute
    {
        //! Check if a symbol is on the plot canvas before painting it.
        ClipSymbols   = 0x01
    };

    //! Paint attributes
    typedef QFlags<PaintAttribute> PaintAttributes;

    explicit OBoxPlotItem( const QString &title = QString() );
    explicit OBoxPlotItem( const QwtText &title );

    virtual ~OBoxPlotItem();

    //~ virtual int rtti() const;

    void setPaintAttribute( PaintAttribute, bool on = true );
    bool testPaintAttribute( PaintAttribute ) const;

    void setSamples( const QVector<OBoxPlotSample> & );
    void setSamples( QwtSeriesData<OBoxPlotSample> * );

    void setSymbolStyle( SymbolStyle style );
    SymbolStyle symbolStyle() const;

    void setSymbolPen( const QColor &,
        qreal width = 0.0, Qt::PenStyle = Qt::SolidLine );
    void setSymbolPen( const QPen & );
    QPen symbolPen() const;

    //~ void setSymbolBrush( Direction, const QBrush & );
    //~ QBrush symbolBrush( Direction ) const;
    void setSymbolBrush( const QBrush & );
    QBrush symbolBrush( ) const;

    void setSymbolExtent( double );
    double symbolExtent() const;

    void setMinSymbolWidth( double );
    double minSymbolWidth() const;

    void setMaxSymbolWidth( double );
    double maxSymbolWidth() const;

    virtual void drawSeries( QPainter *painter,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;

    virtual QRectF boundingRect() const;

    virtual QwtGraphic legendIcon( int index, const QSizeF & ) const;

protected:

    void init();

    virtual void drawSymbols( QPainter *,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;

    virtual void drawUserSymbol( QPainter *,
        SymbolStyle, const OBoxPlotSample &,
        Qt::Orientation, bool inverted, double symbolWidth ) const;

    //~ void drawBar( QPainter *painter, const OBoxPlotSample &,
        //~ Qt::Orientation, bool inverted, double width ) const;

    void drawBoxAndWhisker( QPainter *, const OBoxPlotSample &,
        Qt::Orientation, double width ) const;

    virtual double scaledSymbolWidth(
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect ) const;

private:
    class PrivateData;
    PrivateData *d_data;
};

Q_DECLARE_OPERATORS_FOR_FLAGS( OBoxPlotItem::PaintAttributes )

/*!
    Interface for iterating over an array of quartile samples
*/
class QWT_EXPORT OBoxPlotData: public QwtArraySeriesData<OBoxPlotSample>
{
public:
    OBoxPlotData(
        const QVector<OBoxPlotSample> & = QVector<OBoxPlotSample>() );

    virtual QRectF boundingRect() const;
};

QWT_EXPORT QRectF qwtBoundingRect(
    const QwtSeriesData<OBoxPlotSample> &, int from = 0, int to = -1 );

#endif
