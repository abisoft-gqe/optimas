#ifndef INDEXWINDOW_H_
#define INDEXWINDOW_H_

#include "../ui_indexWindow.h"

#include <QtWidgets>
#include <QDialog>
#include <QTableWidget>
#include "utils.h"
#include "muParser.h"

class IndexWindow : public QDialog {
	Q_OBJECT

	public:
		IndexWindow();
		IndexWindow(QTableWidget* t, QWidget *parent);
		IndexWindow(QMap<QString, QVector<double> >& map_ind_indexres, QMap<QString, QString> & map_formula, const QMap <QString, QStringList>& map_scores, QTableWidget* t, QWidget *parent);
		~IndexWindow();
		QString getFormulaName() const;

	private slots:
		void onCalcButtonClicked();
		void onItemClicked(QTableWidgetItem* item);
		//void delLastInput();
		
		void saveFormula();
		void loadFormula();
		
		void resetFormula();
		void applyFormula();

	private:
		void initWindow();

		// attributes
		Ui::IndexWindow ui;
		const QMap <QString, QStringList>* m_map_scores;
		QTableWidget* table;   // address of the scoretableWidget given in argument of the constructor.
		QString m_name;
		QString m_formula;       // formula entered by the user
		QMap<QString, QString>* m_map_formula; // stores the saved formulas (formula name --> literal expression)
		QMap<QString, QVector<double> >* m_map_ind_indexres;
		mu::Parser p;          // parser/analyser of the formula
		// vector<double> m_vect_values;
		double* m_vect_values; // array storing the values of the variables involved in the index calculation.
		                       // each element is link to a variable within the formula

};

#endif /* INDEXWINDOW_H_ */
