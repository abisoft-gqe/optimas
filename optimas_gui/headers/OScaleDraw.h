#ifndef OSCALEDRAW_H_
#define OSCALEDRAW_H_

/* -*- mode: C++ ; c-file-style: "stroustrup" -*- *****************************
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the Qwt License, Version 1.0
 *****************************************************************************/

#include <qglobal.h>
#include <qcolor.h>
#include <qwt_scale_draw.h>
//~ #include <iostream>
//~ #include <algorithm>

using namespace std;

////////////////////////////////////////////////////////////////////////
// custom scaleDraw for x (bottom) axis :
// inherite QwtScaleDraw
class CustomScaleDraw: public QwtScaleDraw {

	public:
		CustomScaleDraw(){}
		CustomScaleDraw(const QMap<QString, QStringList> & seq_legend, int dec=0) {	// utiliser les templates ?
			QMap<QString, QStringList>::const_iterator it;
			for (it=seq_legend.begin(); it!=seq_legend.end(); ++it) {
				m_vect_legend.push_back(it.key()); //// voir plus bas.
			}
			d=dec;
			//setTickLenths(0,0,0);
		}
		CustomScaleDraw(const QVector<QString> &vect_legend, int dec=0) {	// utiliser les templates ?
			for(int i=0; i<(int)vect_legend.size(); i++){
				m_vect_legend.push_back(vect_legend[i]); //// voir plus bas.
			}
			d=dec;
			//setTickLenths(0,0,0);
		}
		void setTickLengths(int min, int med, int maj){
			setTickLength(QwtScaleDiv::MinorTick, min);
			setTickLength(QwtScaleDiv::MediumTick, med);
			setTickLength(QwtScaleDiv::MajorTick, maj);
			//if (min+med+maj == 0) enableComponent(QwtScaleDraw::Backbone, false);
		}
		// redefinition of QwtScaleDraw::label() virtual method.
		virtual QwtText label(double v) const {
			size_t s = m_vect_legend.size();
			int i = (int)v;
			if (s<1) return QwtScaleDraw::label(v);
			if ( (v != (double)i) || i<d || (size_t)i>=s+d) return QString("");
			return m_vect_legend[i-d];
		}

	private :
		QStringList m_vect_legend; //// la taille est connue => QVector<QString> est mieux.
		int d;
};


#endif /* OSCALEDRAW_H_ */
