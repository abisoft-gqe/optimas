#ifndef HISTOGRAMITEM_H_
#define HISTOGRAMITEM_H_

#include <qglobal.h>
#include <qcolor.h>
#include <qwt_scale_draw.h>
//#include <qwt_plot_item.h> // qwt5
#include <qwt_plot_histogram.h> // qwt6
#include <iostream>
#include <algorithm>

class QwtIntervalData;
class QString;

class HistogramItem: public QwtPlotHistogram
{
public:
    
    HistogramItem( const QString & title = QString()); //// new
    
    // if overriden virtual method declared, it must be defined (even if empty) in the cpp.
    // should get this error if not defined: undefined reference to « VTT for HistogramItem »
    //virtual ~HistogramItem();

    void setValues( uint numValues, const double * values); //// new, for testing only
    void setValues( const QVector<QwtInterval> & intervals, const QVector<double> & values ); //// new
    void setColor(const QColor & color);
};


// custom scaleDraw for x (bottom) axis of the plot
class MyScaleDraw: public QwtScaleDraw {

	public:
		MyScaleDraw(){}
		MyScaleDraw(const std::vector<double> &vect_legend) {
			m_vect_legend = &vect_legend;
			setTickLengths(0, 0, 0);
		}

		void setTickLengths(int min, int med, int maj){
			setTickLength(QwtScaleDiv::MinorTick, min);
			setTickLength(QwtScaleDiv::MediumTick, med);
			setTickLength(QwtScaleDiv::MajorTick, maj);
			if (min+med+maj == 0) enableComponent(QwtScaleDraw::Backbone, false);
		}
		// redefinition of QwtScaleDraw::label() virtual method.
		virtual QwtText label(double v) const {

			std::vector<double>::const_iterator it = lower_bound(m_vect_legend->begin(), m_vect_legend->end(), v);
			if(it == m_vect_legend->end() || *it != v) return QString("");
			if(it == m_vect_legend->begin()) return QString("All");
			return QString().setNum(int(it - m_vect_legend->begin()));
		}

	private:
		const std::vector<double>* m_vect_legend;
};


#endif /* HISTOGRAMITEM_H_ */
