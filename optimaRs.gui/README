This file describes the installation instructions of optimaRs.gui on all supported systems.


IMPORTANT:

optimaRs.gui is a shiny application based on the R package 'optimaRs'.
Therefore make sure to have optimaRs installed on your system before installing optimaRs.gui.
Follow the optimaRs README file from this URL: https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases



======================================================================
=                                                                    =
=      optimaRs.gui installation notes for Windows 10 system         =
=                                                                    =
======================================================================

    __________________

    System requirement
    __________________

    MS Windows 10 64 bits version ≥ 1607 (Redstone 1 released on 2016-08-02)
    optimaRs.gui does not work on Windows 7 (NB: MS ended W7 support on 2020-01-14)
  
    __________________________________________

    INSTALL FROM A PRE-COMPILED BINARY PACKAGE
    __________________________________________
    
    Make sure to have R package 'optimaRs' already installed on your system.
    See the 'installation notes for Windows 10 system' in the optimaRs README file from this URL:
    https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases

    1. We provide several pre-compiled binary packages build on R 4.1.x, R 4.2.x, and R 4.3.x.
       If needed, download and install the adequat R release (≥ 4.1.x) for Windows from this page:
       https://cloud.r-project.org/bin/windows/base/
       NB: make sure to install the 64-bits version

    2. Download an optimaRs.gui binary package for Windows 10, according to your installed R release, from this page:
       https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases
       
    3. Open a R 64-bits session and launch the following commands:
       
       # Installation of R package dependencies
       install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")

       # Installation of optimaRs.gui (depending on the release downloaded and the zip archive location on your device)
       install.packages("/path/to/optimaRs.gui_<release_number>.zip", type="binary", repos=NULL)

    NB: To install optimaRs.gui on older R releases, see the install from source instructions below 

    _______________________

    INSTALL FROM THE SOURCE 
    _______________________

    Building Prerequisite
    =====================

    1. R ≥ 4.1 (although the minimal required R version to run optimaRs.gui is R 4.0.x, these instructions only apply to R 4.1 and later)

       Download and install the last R release (≥ 4.1) for Windows from this page:
       https://cloud.r-project.org/bin/windows/base/
       NB: make sure to install the 64-bits version

    2. Rtools 4.x

       Download and install the 64-bits version using the apropriate link on this page :
       https://cran.r-project.org/bin/windows/Rtools/

    Install optimaRs.gui from source
    ================================

    Make sure to have R package 'optimaRs' already installed on your system.
    See the 'installation notes for Windows 10 system' in the optimaRs README file from this URL:
    https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases
       
    1. Install R package dependencies :
      
       # Open a R session and launch the following command:
       install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")

    2. Download the optimaRs.gui source package from the section 'Source packages' on the last release page:
       https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases
    
    (3. Open a Rtool 4.x terminal window (windows menu > Rtools 4.x > Rtools MinGW 64-bit or Rtools bash))
    
    4. Install optimaRs.gui:
       # Open a R session where you downloaded the source package and launch the following command:
       install.packages("optimaRs.gui-x.y.tar.gz", type="source", repos=NULL, INSTALL_opts="--no-multiarch")
       # Where x.y corresponds to the release number

    __________

    ADDITIONAL
    __________
                  
    Build a 64-bits binary package for Windows 10 and R 4.2
    =======================================================

    Open Rtool42 terminal (windows menu > Rtools42 > Rtools42 bash)

    Change to the directory where you downloaded the optimaRs.gui source archive (e.g. ~/Downloads):

      cd ~/Downloads

    Extract the source archive (exemple with a tar.gz archive)

      tar xzf optimaRs.gui-<release_number>.tar.gz

    Change to the extracted directory
      
      cd optimaRs.gui-<release_number>

    Open a R 4.2 (or later) 64-bit session (path to R executable depends on the chosen directory during R installation)
      
      'C:\Program Files\R\R-4.2.x/bin/x64/R'
      
    and launch the following commands (install R package devtools if needed):
      install.packages("devtools", repos="https://cloud.r-project.org")
      install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")
      library(devtools)
      build(binary=TRUE, args='--no-multiarch')
     
    This will create a binary installation package in the parent directory: optimaRs.gui_<x.x>.zip




======================================================================
=                                                                    =
=    optimaRs.gui installation notes for modern GNU/Linux systems    =
=                                                                    =
======================================================================

    __________________

    System requirement
    __________________

    Modern GNU/Linux distributions (tested on Debian 10 and 11, Ubuntu 20.04, Fedora 36 and CentOS 7)

    _______________________

    INSTALL FROM THE SOURCE
    _______________________

    Building Prerequisite
    =====================

    1. GNU compiler collections with c++ support (g++) version 6 or later

       Check the version installed with this command in a terminal window:
       g++ --version
       or
       c++ --version
      
    2. system libraries libicu and zlib
      
       We STRONGLY recommend to install these libraries via the package manager of your GNU/Linux distribution.

       e.g. (prepend the command with the sudo statement if installing as sudoer):

       On Debian/Ubuntu/Mint, as root or sudoer:
       apt-get install libicu-dev pandoc zlib1g-dev

       On Redhat/CentOS/Fedora, as root or sudoer (use yum instead of dnf on old release):
       dnf install libicu-devel zlib-devel

    3. R version 4.x.x or later

       https://www.r-project.org/
    
 
    Install optimaRs.gui from source
    ================================

    Make sure to have R package 'optimaRs' already installed on your system.
    See the 'installation notes for modern GNU/Linux systems' in the optimaRs README file from this URL:
    https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases

    1. Install R package dependencies:
      
       # Open a R session and launch the following command:
       install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")

    2. Download the optimaRs.gui source package from the "Source packages" section on the last release page:
       https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases
    
    3. Install optimaRs.gui:
       # Open a R session in the directory where you downloaded the source package and launch the following command:
       install.packages("optimaRs.gui-src-x.y.tgz", type="source", repos=NULL)
       # Where x.y corresponds to the release number

    NB: How to solve missing system library issues
    ----------------------------------------------
    Here is the kind of message displayed during the installation process
    when a system library is missing (example with the R package curl):
    
    ///////////////////////////////////////////////////////////////////////
    * installing *source* package ‘curl’ ...
    ** package ‘curl’ successfully unpacked and MD5 sums checked
    ** using staged installation
    Package libcurl was not found in the pkg-config search path.
    Perhaps you should add the directory containing `libcurl.pc'
    to the PKG_CONFIG_PATH environment variable
    Package 'libcurl', required by 'virtual:world', not found
    Package libcurl was not found in the pkg-config search path.
    Perhaps you should add the directory containing `libcurl.pc'
    to the PKG_CONFIG_PATH environment variable
    Package 'libcurl', required by 'virtual:world', not found
    Using PKG_CFLAGS=
    Using PKG_LIBS=-lcurl
    --------------------------- [ANTICONF] --------------------------------
    Configuration failed because libcurl was not found. Try installing:
     * deb: libcurl4-openssl-dev (Debian, Ubuntu, etc)
     * rpm: libcurl-devel (Fedora, CentOS, RHEL)
    If libcurl is already installed, check that 'pkg-config' is in your
    PATH and PKG_CONFIG_PATH contains a libcurl.pc file. If pkg-config
    is unavailable you can set INCLUDE_DIR and LIB_DIR manually via:
    R CMD INSTALL --configure-vars='INCLUDE_DIR=... LIB_DIR=...'
    ///////////////////////////////////////////////////////////////////////
    
    - First focus on the line identifying the missing library name (libcurl in this example):
      "Configuration failed because libcurl was not found."
    
      and spot the package name to install depending on the Linux distribution you are using
      " * deb: libcurl4-openssl-dev (Debian, Ubuntu, etc)"
      " * rpm: libcurl-devel (Fedora, CentOS, RHEL)"
    
    - Then open a terminal window and install it as root or sudoer (prepend the command with the sudo statement if installing as sudoer).
      On Debian or Ubuntu:
      apt-get install libcurl4-openssl-dev
      On Fedora, CentOS, RHEL (replace dnf with yum if you are using an old release):
      dnf install libcurl-devel




======================================================================
=                                                                    =
=        optimaRs.gui installation notes for modern macOS systems        =
=                                                                    =
======================================================================

    __________________

    System requirement
    __________________

    MacOS release ≥ 10.13 (High Sierra)
      
    __________________________________________

    INSTALL FROM A PRE-COMPILED BINARY PACKAGE
    __________________________________________
    
    Make sure to have R package 'optimaRs' already installed on your system.
    See the 'installation notes for modern macOS systems' in the optimaRs README file from this URL:
    https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases

    1. Download and install the last R release (>= 4.2.x) for macOS from this page:
       https://cloud.r-project.org/bin/macosx/base/

    2. Download the last binary optimaRs.gui release for macOS from this page:
       https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases
       
    3. Open a R 64-bits session and launch the following commands:
      
       # Installation of R package dependencies
       install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")

       # Installation of optimaRs.gui (depending on the release downloaded and the tar.gz archive location on your device)
       install.packages("/path/to/optimaRs.gui_<release_number>.macos.tgz", type="binary", repos=NULL)

    NB: To install optimaRs.gui on older R releases, see the install from source instructions below 
    
    _______________________

    INSTALL FROM THE SOURCE 
    _______________________

    Building Prerequisite
    =====================

    1. Clang compilers with c++ support (clang++)

       clang++ may already be installed on your macOS system.
       Check the installed version using this command in a terminal window: clang --version

       If you need to install or update the Clang compilers,
       execute the command: xcode-select --install
       and follow the instructions.

    2. R version 4.0.x or later

       https://cloud.r-project.org/bin/macosx/base/

    Install optimaRs.gui from source
    ================================
    
    Make sure to have R package 'optimaRs' already installed on your system.
    See the 'installation notes for modern macOS systems' in the optimaRs README file from this URL:
    https://forgemia.inra.fr/abisoft-gqe/optimas/-/releases

    1. Install R package dependencies :
      
       # Open a R session and launch the following command:
       install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")

    2. Download the optimaRs.gui source package from the section asset on the last release page:
       https://forgemia.inra.fr/abisoft-acep/optimas/-/releases
    
    3. Install optimaRs.gui:
       # Open a R session in the directory where you downloaded the source package and launch the following command:
       install.packages("optimaRs.gui-x.y.tar.gz", type="source", repos=NULL)
       # Where x.y corresponds to the release number

    __________

    ADDITIONAL
    __________
                  
    Build a 64-bits binary package for macOS release ≥ 10.13 (High Sierra) and R 4.2
    ================================================================================
    
    Change to the directory where you downloaded the optimaRs.gui source archive (e.g. ~/Downloads):

      cd ~/Downloads

    Extract the source archive (exemple with a tar.gz archive)

      tar xzf optimaRs.gui-src-<release_number>.tgz

    Change to the extracted directory
      
      cd optimaRs.gui

    Open a R 4.2 (or later) 64-bit session (path to R executable depends on the chosen directory during R installation)
      
      R
      
    and launch the following commands (install R package devtools if needed):
      install.packages("devtools", repos="https://cloud.r-project.org")
      install.packages(c("shiny","shinydashboard","shinyBS","shinyFiles","DT","ggplot2","colourpicker"), repos="https://cloud.r-project.org")
      library(devtools)
      build(binary=TRUE, args='--no-multiarch')
     
    This will create a binary installation package in the parent directory: optimaRs.gui_<version>.tgz,
    where <version> corresponds to the version number set in the DESCRIPTION file (line strating with Version:) 
    
