---

# optimaRs.gui user guide

---

To show and use the full functionalities of optimaRs.gui, the present documentation will focus on real data coming from a multiparental marker-assisted recurrent selection (MARS) study (Blanc et al., 2006, 2008).

Six connected F2 populations, with 150 individuals each, were obtained from a half-diallel design between four unrelated maize inbred lines (DE, F283, F810 and F9005). Eleven QTL were detected for silking date. A set of 34 markers was selected with at least three markers (microsatellites) to follow each QTL (see blanc.map & blanc.dat files). Two cycles of MARS were performed with each time a step of selfing before intermating. In this example, optimaRs.gui is used at the last cycle to select the best individuals (among 297 genotyped plants) that will be used for the next cycle of MARS (see below).

>![***Figure 6: multiparental MAS study (Blanc et al., 2006, 2008) used as example within optimaRs.gui***](user_guide_images/fig6.png)

&nbsp;
<div style="page-break-after: always;"></div>

## Running optimaRs.gui

Open a R session (either a basic terminal session or via your favorite R IDE like Rstudio), and execute these commands:

```R
library(optimaRs.gui)
launch.optimaRs.gui()
```

&nbsp;

## Step1 - Computation of genotypic probabilities - Prediction of genetic values

The first step consists in computing the probabilities of IBD alleles transmission throughout generations and estimate the individuals genetic values.

<u>Notes</u>: Instead of running a complete prediction process, you can alternatively load the results from the multiparental example data set, provided with the package.  
To do so, select ***Data > Reload Multiparental Data*** from the top menu bar.  

Otherwise, follow the next section *Running the prediction process* just below.

&nbsp;

### Running the prediction process

Before running an analysis, you must specify the paths to the genetic map file, the genotypes/pedigree file, and output directory.  
Select **Data > Import Text Data** from the top menu bar as below. Then use the **Browse** buttons to set the paths.

>![***Figure 5: data set importation to run the program***](user_guide_images/fig5.png)

**Map file**: path to the genetic map input file (.map).  
<u>Remarks</u>: QTLs information files and map file has to be in the same folder, and share the same base name, e.g. blanc.map blanc.qtlpos and blanc.qtll. Note that the method used to determine markers flanking the QTL is determined by the file(s) found in the same folder. If several files describing markers to be used to track QTL are present in the directory, only one will be used, according to priority rule .qtll > .qtln > .qtlw.  
**Genotype file**: path to the genotype/pedigree file (.dat).  
**Allelic effects file** (optional, can be loaded/refreshed after loading mandatory files): path to the allelic effects file. May be located in any folder.  
See allelic effects section for more details on its structure.  
**Output directory**: path to the folder where the results will be stored. Results from each run will be stored within a new dated directory created automatically within this folder. Note that your output directory should not be in the "Program Files" folder or other specific directories with administrator privileges.

<u>Advanced options/parameters</u>:

**QTL analyzed**: by default all the QTL present in the input files will be analyzed. You can also choose to select a specific QTL to run the analysis.  
**Cut-off - Diplotypes**: genotypic probability below which a rare phased genotype (diplotype) is removed and no more considered in subsequent computations (default value = 0.000001).  
**Cut-off - Gametes**: (default value = 0.000000). It corresponds to the probability that the number of crossovers expected in the region between flanking markers exceeds a given value. Thus, unlikely gametes with number of crossovers over this value are removed and no more considered in subsequent computations. Use of this option with values up to 0.01 is recommended in case many flanking markers per QTL lead to high computation time with default option.  
**Verbose**: Verbose mode creates two files per QTL position, reporting respectively gamete and diplotype probabilities for all individuals (default value = OFF).  
<u>WARNING</u>: With large/complex data, both files may take a lot of disk space, it is then recommended to leave this option disabled.

&nbsp;

**Click the *Run* button to launch the prediction process on this data set**.

At the end of the process, the bottom area of the window will show this information message:

>*Prediction process finnished*.

Otherwise, if an error occurs during the process, it will show a specific error message.

Result files are created in the specified folder.  

Close the "Import data..." window by pressing the "Close" button.

&nbsp;

To visualize and analyze the results, **optimaRs.gui** includes three modules accessible from the left side panel icons,  
and corresponding to the different steps of the selection program.

>![ ](user_guide_images/fig5bis.png)

&nbsp;

Step 1 results are displayed via three tables - *Molecular score*, *Homo/Hetero*, and *Parental allele probabilities* - and graphs.

>![***Figure 7: genetic value (molecular score) for each individual (all/each QTL)***](user_guide_images/fig7.png)

In the **Molecular score** table, individuals are represented in lines followed by their pedigree, the cycle of selection and the group they belong to.  
Plants can be sorted regarding their value for any columns by clicking on the column headers.  
The genetic value (MS column, i.e. the average expected proportion of favorable allele at the QTL positions), varies between 0 and 1. A value of "1" indicates that the individual corresponds to the targeted ideotype, it is homozygous for the favorable allele(s) at all the QTL.

In this maize example, MS varies between 0.27 (X: parental line) and 0.83 (B8: individual not presented in Fig. 7, coming from the last cycle of selection). Note that the four inbred lines D (DE), F (F283), S (F810) and X (F9005) have a MS of 0.36, 0.36, 0.45 and 0.27 respectively.

More detailed genotypes can be displayed by ***double clicking on MS or QTL cells*** (see below). This view summarizes and aggregates the information presented in the two other tables (Homo/Hetero and Estimation of parental alleles probability).

>![***Figure 8: detailed genotype in terms of parental alleles at QTL position***](user_guide_images/fig8.png)

&nbsp;

Fig. 8 shows that individual ***B8*** has a molecular score of ***0.875162*** at ***qtl4***.  
It has a probability of ***0.763269*** to be homozygous for the favorable alleles ***s/f*** i.e. *Homo(+/+)*.  
This score corresponds to the sum of the probabilities of the genotypes ***f:f=0.522333, s:f=0.225659*** and ***s:s=0.015277***.  
Its ***MS*** of ***0.8752*** corresponds to the expected proportion of favorable allele(s), i.e. *Homo(+/+)* + 1/2 Hetero(+/-)*.  
Founders (represented by d, f, s and x alleles) indicate the expected proportion of parental alleles. We notice that this individual is issued from three parental lines D, F, S and not X (see also [pedigree](#pedigree_mark) graphs).

&nbsp;

A colored view of the molecular score table can be displayed to identify more easily QTL for which a given individual is considered as fixed or not (see Fig. 9).  
Press ***Visualization > Set Color Scheme*** in the top menu bar.

>![***Figure 9: colored view of the molecular score table***](user_guide_images/fig9.png)

A value of 0.75 (by default) is selected for the probability threshold to be considered as homozygous (un)favorable or heterozygous at the QTL positions. A color can be assigned to each of them. Genotypes which are not assigned to any of these categories are considered "uncertain genotypes (?)". When you apply a new set of parameters (cut-off / colors), the four corresponding columns (No.(+/+), No.(-/-), No(+/-), No.(?)) of the MS table are updated.

The figure below show that individual B8 is considered as homozygous favorable for eight blue QTLs => No.(+/+) = 8, homozygous unfavorable for only one red QTL => No.(-/-) = 1, it presents no heterozygous yellow QTL => No.(+/-) = 0, and two QTLs in white are uncertain => No.(?) = 2

![ ](user_guide_images/fig9bis.png)

Some MS at QTL positions are close to 1 (e.g. qtl3 = 0.9761) and some others are lower (e.g. qtl2 = 0.8598). This uncertainty can be due to (i) the fact that one marker flanking the QTL is heterozygous whereas the other one is homozygous favorable, which indicates that a recombination took place near the QTL position, or (ii) that there are missing data (see genotypes/pedigree file).

&nbsp;

The results of the different tables can be visualized on graphs that are automatically generated by clicking on the Graphs tab (see below).

>![***Figure 10: distribution of QTL MS, global genetic values and their evolution over the different cycles of selection***](user_guide_images/fig10.png)

The graph on Fig. 10a indicates the frequency of favorable alleles at the different generations of selection (on average and for each QTL). Note that no genetic gain is expected for the last generation (C2, in blue) because individuals are not selected yet.

Fig. 10b and 10c show the distribution of the molecular score (for each QTL separately and on average for all QTL) whereas Fig. 10d displays the average of the MS for individuals classified according to another classification criterion (e.g. subprogrammes, families, etc).  
All the graphs can be exported (in png, svg or pdf formats) by using the ***"Export"*** button.

&nbsp;

In the estimation of the Molecular Score (MS), optimaRs.gui attributes the same weight to all QTL declared in the map file. It is also possible to discard QTL and/or to attribute economical weights defined by the breeder, to compute a "Weight" index.  
Select ***Tools > QTL Weight*** to open a dialog window (see below).

>![***Figure 11: weighted molecular score give more or less importance to the different QTL***](user_guide_images/fig11.png)

We noticed in this example that the favorable allele (x) at **qtl1** may be lost because **(i)** the ten best individuals of the panel have a molecular score of 0 at **qtl1** (see Fig. 11, cells in red) and **(ii)** graphs in Fig. 10a and 10b indicate the same decay. Thus, a weight of 3.0 has been attributed to the **qtl1**. ***Assign QTL weights then press Apply***. It will result in an update on the "MS_Weight" column and therefore produce a new classification of individuals.

In addition to the ***MS*** and ***MS_Weight*** columns, optimaRs.gui  estimates an "Utility Criterion" (***MS_UC*** column) which evaluates the expected value of superior gametes of each individual by combining the molecular score with the expected variance of the MS of its gametes.

Allelic effects of QTL for traits of interest can be provided by the user.  
Select ***Data > Import Text Data*** from the top menu bar, browse the allelic effects file and click ***"proceed"***.  
optimaRs.gui then computes predicted molecular scores (***PMS***) for each trait documented with allelic effects and adds/replaces the corresponding columns (indicated by "PMS" prefix) on the right side of displayed table. Note that missing information "-" will be considered as zero.

Indexes can be defined by combining different sources of information (MS, MS_UC, QTL, PMS and quantitative trait information provided by the user).  
To do so, Select ***Tools > Compute index, define formula and click Apply*** (Figure 12 below).  
By default, the index column will be identified as ***IndexN*** (where N is an automatically incremented integer value, as several index columns may be computed), but an alternative name can be chosen by the user by prepending the literal expression with this alternative name and the character '='.  
Once applied, a formula can be reused, removed or modified later in the same optimaRs.gui session, or in a further one if the data have been saved (see [Saving/reloading session](#save_reload)).

>![***Figure 12: the resulting index column is always located just before the MS column.***](user_guide_images/fig12.png)

&nbsp;

The ***"Search"*** text box can be used to filter the content of the table according to the pattern entered (see below).  
The pattern is searched among the whole table (columns and rows). Only the matching rows will be shown.

>![***Figure 13: filter rows according to a search pattern***](user_guide_images/fig13.png)

&nbsp;

## Step2: Selection of individuals

Taking into account all the information coming from the previous tables, we can select individuals to produce the next generation.

&nbsp;

### Methods for selection step

*optimaRs.gui* provides three different ways to select individuals

&nbsp;

<u>**Manual selection**</u>: selection of individuals based on your own judgment (see Fig. 15).  

From the main **Molecular score** table (step 1 window),  
- select individuals via a combinaison of ***Ctrl+click*** and/or ***Shift+Click*** keys, as you would do in your OS file explorer  
- press ***Right click***  
- in the ***Add to list*** dialog window, enter the name of the new list in the text zone (e.g. ***List1_Manual_Selection*** as in the screenshot below) or select an existing list  
- Click ***OK***.

>![***Figure 15: manual selection of individuals added to a list***](user_guide_images/fig15.png)

Individuals are selected manually [ **a** ] and then added to a list of your choice [ **b** ]. This new list can be accessed through the Step 2 interface [ **c** ].

Click on the **Step 2 icon** to see the list(s) of selected individuals on the ***"Selection"*** page and access the two next selection methods.

<u>Notes</u>:
Here you can create new empty lists by clicking the **"Add" button**.  
To **rename** a list **double click on its name**, write the new name and **press "Enter"**.  
To **remove** some list(s), select the list(s), and click the **"Remove" button**.

&nbsp;

<u>**Truncation selection (TS)**</u>: individuals can be ranked automatically based on four possible criteria: **MS, MS_Weight, MS_UC, Index** (if computed).

As an example, create an empty list by ***clicking the "Add" button*** in the left panel and double click on this list to rename it ***List2_Truncation_Selection***.  

Then apply the **Truncation Selection method** as described below:

1. Select the **Trucation method** and click **settings**
2. Set the parameters as shown in the Fig. 16 below: **N = 8, Criterion = MS, Cycle = C2, Destination List = List2_Truncation_Selection**.
3. Press ***Apply***.

>![***Figure 16: truncation selection based on three criteria (MS, Weighted MS and Utility Criterion)***](user_guide_images/fig16.png)

The eight best individuals (according to the chosen criterion) are selected to generate the next population.

However the colored view points that <u>we are losing the favorable allele for **qtl1**</u> (in red, qtl1 = 0 for all individuals).  

&nbsp;

<u>**QTL complementation selection (QCS)**</u>: takes into account complementarities between candidate individuals regarding the favorable alleles they carry (see figures below).

It aims at preventing the loss of rare favorable allele(s) (Hospital et al., 2000).  
This option completes an initial list with supplementary individuals bringing the complementary alleles, it is especialy useful when a high number of QTL is considered.

As an example, first create another empty list and rename it ***List3_Complementation_Selection***.

Then re-apply the **Truncation Selection** method (N = 8, Criterion = MS, Cycle = C2, Destination List = List3_Complementation_Selection).

At this step eight individuals are selected.

To complete the list with the QCS procedure:

1. Select the **QTL complementation** method and click **settings**
2. In the dialog window, set up the parameters as shown in Fig. 17
3. Press “Apply”.

>![***Figure 17: QTL complementation selection***](user_guide_images/fig17.png)

In this example, QCS adds two candidates such that their QTL composition complements those eight individuals already selected (see Fig 17).

<u>The QCS is described by five parameters</u>:

**θ<sub>MS</sub>**: corresponds to the MS QTL threshold (QTLx > 0.47 by default), above which a favorable QTL allele is declared "present". In this case and depending on the threshold value, not only individuals considered as homozygous for the favorable allele at QTL position will be taken into account (e.g. heterozygous individuals, see Fig. 17, in yellow).

**nT**: means that each QTL favorable allele is requested to be "present" in at least nT selected individuals (here nT = 2).

**MS<sub>min</sub>**: the minimum threshold value (MS<sub>min</sub> ≥ 0, by default) for the addition of an individual. In this example, individuals with MS (genetic value) < 0.7 are not considered.

**N<sub>max</sub>**: the maximum number of individuals selected at the end of the QCS process. Here, up to two individuals will be added to the final subset of selected individual (N<sub>max</sub> = 10).

**Cycle/Group**: optional information regarding the generation of selection and another classification criterion in the programme (e.g. first cycle, second cycle, F2, F4, subprogrammes, families, etc). "C2" (cycle 2) was selected instead of "None" (no selection, by default) in order to select individuals that belong to the last cycle of selection.

This approach can be applied to any predefined list (including the possibility to consider an empty list). In this example, the first eight individuals were selected via the truncation selection (based on the MS criterion).  
Then: **(i)** the QTL for which the favorable alleles are "present" in fewer than **nT selected individuals** are identified; **(ii)** among the remaining individuals (belonging to **Cycle**), taken in order of decreasing MS (with MS ≥ MS<sub>min</sub>), the process searches for the individual having favorable alleles "present" at the largest number of those QTL identified in (i). This individual is added to the subset of selected individuals (i.e. N = 8 + 1).  
Steps (i) and (ii) are iterated until either of the following conditions is met: favorable alleles at all QTL are present in at least nT individuals of the selected subset, or the number of individuals in the selected subset reaches the given N<sub>max</sub> value, or it is not possible to find an individual in step (ii).

Note that in step (ii), MS is a secondary criterion; individuals are taken based on their ability to complement the subset of already selected individuals. Thus, with the present set of parameters, two individuals (**B124, B125**) have been added to the selected list.

&nbsp;

### 2. Comparison of lists of selected individuals

The different lists of selected individuals can be compared in two parallel tables (see below) displayed by default (which can be reduced to one list by clicking on the **"Hide right table"** button).

>![***Figure 19: comparison between lists of selected individuals (via tables)***](user_guide_images/fig19.png)

If we compare the two lists of individuals, one created via truncation selection (on the left) and the other created via QCS (on the right), we see that the individuals B124 and B125 added in the QCS list will bring two more unfavorable alleles (qtl5 and qtl8 in red, not presented in Fig. 19) for the next generation.

Note that individuals can be deleted from a list by **manually selecting the individuals** and press the **"Remove selected rows"** button.  
Selected individuals can also be copied to another list.  
To do so, **right-click on the selected individuals**, select an existing list in the **"Add to List"** dialog window, and click **OK**.

&nbsp;

The different lists of individuals can also be compared via the Graphs window (click on the Graphs tab, see Fig. 20).

>![***Figure 20: comparison between lists of selected individuals (via graphs)***](user_guide_images/fig20.png)

The left side graph shows that the truncation selection based on MS leads to the selection of individuals all carrying the unfavorable allele at **qtl1**.  
On the right side graph, the two individuals (B124, B125) added via the QCS procedure are observed with a MS comprised between 0.45 and 0.5 for **qtl1**.  
If we select qtl7 in both lists (not represented in Fig. 20), we notice that the qtl7 will be fixed for the favorable allele at the next generation (MS > 0.95 for all individuals).

To visualize the origin of the selected individuals of each list, the user can display their pedigree (see below). Move to **"Pedigree"** table, and select the list of your choice.<a id="pedigree_mark"></a>

>![***Figure 21: pedigree of individuals selected via the Truncation Selection method***](user_guide_images/fig21.png)

It is possible to compare pedigrees coming from different lists of selection (see Fig. 21 & 22, Truncation Selection list vs. QCS, respectively).  
Note that the two individuals added via the QCS bring the **parental allele x**.  
So, if we use the QCS list to produce the next generation, the four parental alleles will be present in the next cycle.

>![***Figure 22: pedigree of individuals selected via the QCS method***](user_guide_images/fig22.png)

This representation is useful to follow the contribution of selected individuals over generations of selection and to prevent possible bottlenecks (individuals coming from a reduced number of parents at a given generation), in order to limit risk of drift (which may lead for instance to the fixation of an undesired phenotypic type for traits not considered in the MARS process).  
It also can be used to maintain diversity for selection on traits complementary to those considered for the MARS process.

&nbsp;

## Step 3: Identification of crosses to be made among selected individuals

Now that your list(s) of selected individuals is/are established, it is necessary to identify the crosses to be made to initiate the next MARS cycle.  
We addressed crosses between individuals of a single list (diallel design) or two complementary lists (factorial design).

The diallel situation can be managed with three options:

1. The automatic definition of the whole list of possible crosses according to a ***half-diallel*** (aka complete method, see Fig. 23, 24, 25 and 26).

2. The ***better-half*** strategy (Bernardo et al., 2006) which consists of avoiding crosses between selected individuals with the lowest scores (see Fig. 23, 24, 25 and 26).

3. Application of constraints on the contribution of parents or on the maximum number of crosses (see Fig. 27 and 28).  
In this last case, best crosses are determined according to either the (weighted) molecular score or the utility criterion. Selfing of selected individuals can be included or not.

Then, lists of crosses created via the different methods can be analyzed and compared via graphs.

&nbsp;

Example of instructions to create the two lists of crosses that will contain results of the *half-diallel* and the *better-half* processes among the previously selected candidates:

In the same way as in step 2, create two empty lists of crosses, and rename them ***List6_Half_Diallel_from_QCS*** and ***List7_Better_Half_from_QCS***.

Then click on the ***"Crossing settings"*** button to set up the parameters.

Now choose a list of selected candidates coming from the step 2 (e.g. *List3_Complementation_Selection*) for ***Selection list 1***,  
choose method ***complete*** and ***List6_Half_Diallel_from_QCS*** as ***Destination list*** then click ***Apply*** to run the process and fill *List6_Half_Diallel_from_QCS*.

Then, in the same windows choose method ***better half*** and ***List7_Better_Half_from_QCS*** as ***Destination list*** as shown in Fig. 23 below, then click ***Apply***.

>![***Figure 23: crossing schemes settings***](user_guide_images/fig23.png)

Finally, close the *Crossing settings* window to see the results of these crosses in the dual pane (see below).

>![***Figure 24: comparison of the two lists (half-diallel vs. better-half) of couples generated***](user_guide_images/fig24.png)

On the left table (List6_Half_diallel_From_QCS), all the individuals were crossed together. For each of the 45 resulting pairs, a virtual individual is created and its expected molecular score for all and each QTL is computed.

On the right table (List7_Better_Half_from_QCS), the better-half strategy leads to 25 couples (see also Fig. 25).

<u>Note</u>: Crosses can be deleted from a list in the same way as in step 2 (select rows and press the *"Remove selected rows"* button).  
They also can be copied to another list by right-clicking on the selected rows and selecting a destination list in the *"Add to crossing list"* dialog window.

Meanwhile, graphs are automatically generated to display a view of the selected crosses based on the different strategies (see ***Graphs*** tab).

>![***Figure 25: half-diallel vs. better-half strategies***](user_guide_images/fig25.png)

In the figure above, individuals are ranked on the two axes based on their genetic value (MS from highest to lowest). On the left side, the “Complete” procedure (half-diallel) has been applied.  
All the individuals were crossed together with no limitation on their contribution.  
On the right side (better-half method), crosses between individuals having lowest MS have been avoided (i.e. B37 to B125).

Lists of crosses created via the different methods and/or constraints can also be analyzed and compared via histograms (select ***Graph = Histo*** as below).

>![***Figure 26: comparison of the two lists (half-diallel Vs better-half) of couples generated***](user_guide_images/fig26.png)

As expected from the higher relative contribution of individual B8, the average MS is higher for Better Half option (0,7563178 vs. 0,773952),  
 and the variance of MS among generated couples is lower (6.37 10<sup>-4</sup> vs. 3.51 10<sup>-4</sup>).

Constraints on the contribution of parents or on the maximum number of crosses to be done can be applied (see Fig. 27, 28).  
In this case crosses determination is optimized based on the expected MS or UC of the crosses.  
This will be exemplified with two new lists of crosses:  
Press two times the *"Add"* button to create two new lists of crosses and rename them ***List4_MS_Contrib_1*** and ***List5_UC_Contrib_1***.  
Then press the "*Crossing settings*" button to set the adequat parameter as in Fig. 27 below (*List_UC_Contrib_1* case not represented here).

>![***Figure 27: crossing schemes options (constraints)***](user_guide_images/fig27.png)

To apply this strategy to candidates previously selected via the QCS method, select ***List3_Complementation_Selection*** for **Selection list 1**, and ***List4_MS_Contrib_1*** as **Destination list**.  
To specify that each of the ten candidates must be crossed only once and that there is no constraint on the maximum number of crosses to be done, select ***Contribution of individuals = 1*** and leave ***Number of crosses = maximum***. Select ***Criterion = MS*** and press the “Apply” button.

Then apply the same constraint but change ***Criterion = UC*** and ***Destination list = List5_UC_Contrib_1*** to create the second crossing list (not represented here).

>![***Figure 28: constraints on the contribution of parents based on the MS***](user_guide_images/fig28.png)

&nbsp;

A factorial design between two lists of selected plants can be applied.  
In the "Crossing settings" window, checking the box between ***Selection list 1*** and ***Selection list 2*** enables the possibility to select a second list of selected candidates.

Thus, a list containing only two individuals ***List4_B124_B125*** has been crossed with the eight individuals having the highest MS (***List2_Truncation_Selection***) resulting in a factorial design displayed in Fig. 29.

>![***Figure 29: factorial design applied between two lists of selection***](user_guide_images/fig29.png)

&nbsp;

## Saving/Reloading an optimaRs.gui session <a id="save_reload"></a>

&nbsp;

The whole data generated threw the three steps of an **optimaRs.gui** session can be saved into a unique RData file and reloaded later.

To save the session in RData format, on the top menu bar, select ***Data > Save to RData*** and browse to the destination directory and filename in the file explorer window.

>![***Figure 30: saving the current analysis***](user_guide_images/fig30.png)

&nbsp;

To reload a previously saved optimaRs.gui session, select ***Data > Reload Saved RData*** from the top menu bar and browse to the RData file location, then click the ***Select*** button.

>![***Figure 31: reloading a previous analysis***](user_guide_images/fig31.png)




