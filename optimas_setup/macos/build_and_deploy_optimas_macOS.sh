#!/bin/bash

#sudo port uninstall --follow-dependencies geany
#export GVBINDIR=/Users/macuser/Dev/C++/optimas_gui/optimas_gui.app/Contents/Frameworks
#export DYLD_LIBRARY_PATH=/Users/macuser/Dev/C++/optimas_gui/optimas_gui.app/Contents/Frameworks

#cd /opt/local/lib ; sudo ln -s /opt/local/lib/graphviz lib (can't find pango.lib)
cd ../optimas
make all
cd ../optimas_gui
cp ../optimas/bin/optimas software/

# check the OS
if [[ "Darwin" = `uname` ]]
then
	echo "changing font-size"
	for fic in ui/*.ui # change font size : 9pt to 13pt and 14pt to 20pt
	do
		[[ ! -f ${fic}.pc ]] && mv $fic ${fic}.pc # rename the original file.ui to file.ui.pc if needed.
		# font size 9pt => 13pt and 14pt => 20pt
		perl -ne 'my %h=("9","13","14","20"); s/font-size:(9|14)pt/font-size:$h{$1}pt/g; print' ${fic}.pc > $fic
	done
	# The cellinfo help message become: Press "Alt + Command key" and "click" to show detailed genotypes at QTL position.
	# perl -i -ne 's/Press \"Alt Gr\" and \"left-click\"/Press \"Alt + Command key\" and \"click\"/; print' ui/mainWindow.ui 

	echo "building OptiMAS v1.5"
	qmake optimas_gui.pro
	make
	echo "Making OptiMAS v1.5 bundle application"
	macdeployqt optimas_gui.app
	cp -rf doc images input output software website optimas_gui.app/Contents/MacOS/
	cp -f /opt/local/lib/graphviz/config6 optimas_gui.app/Contents/Frameworks
	cp -rf /opt/local/lib/Resources/qt_menu.nib optimas_gui.app/Contents/Resources/
	cp -f /opt/local/lib/pango/1.6.0/modules/pango-basic-fc.so optimas_gui.app/Contents/Frameworks/
	cp -f ../optimas_setup/libiconv.2.dylib.patched optimas_gui.app/Contents/Frameworks/libiconv.2.dylib
	cp -rf /opt/local/etc/fonts optimas_gui.app/Contents/Frameworks/
	cp -f ../optimas_setup/optimas_gui.v1.5 optimas_gui.app/Contents/MacOS/
	sed -i .old 's/<string>optimas_gui<\/string>/<string>optimas_gui.v1.5<\/string>/' optimas_gui.app/Contents/Info.plist
	rm -rf optimas_gui.app/Contents/Info.plist.old
	echo "done !"
	# MacOS X does not automatically detect that the Eclipse.app's Info.plist has changed. Therefore you need to force update the LaunchService database in the Terminal by using the lsregister command:
	#/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -v -f /Users/macuser/Dev/C++/optimas_gui/optimas_gui.app
	#optimas_gui.app/Contents/MacOS/optimas_gui
fi
